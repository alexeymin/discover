# translation of plasma-discover-notifier.pot to Esperanto
# Copyright (C) 2015 Free Software Foundation, Inc.
# This file is distributed under the same license as the discover package.
# Oliver Kellogg <okellogg@users.sourceforge.net>, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: discover\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-01-10 01:35+0000\n"
"PO-Revision-Date: 2023-12-20 07:31+0100\n"
"Last-Translator: Oliver Kellogg <okellogg@users.sourceforge.net>\n"
"Language-Team: Esperanto <kde-i18n-eo@kde.org>\n"
"Language: eo\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: translate-po (https://github.com/zcribe/translate-po)\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: notifier/DiscoverNotifier.cpp:154
#, kde-format
msgid "View Updates"
msgstr "Vidi Ĝisdatigojn"

#: notifier/DiscoverNotifier.cpp:263
#, kde-format
msgid "Security updates available"
msgstr "Disponeblaj sekurecaj ĝisdatigoj"

#: notifier/DiscoverNotifier.cpp:265
#, kde-format
msgid "Updates available"
msgstr "Ĝisdatigoj haveblaj"

#: notifier/DiscoverNotifier.cpp:267
#, kde-format
msgid "System up to date"
msgstr "Sistemo ĝisdatigita"

#: notifier/DiscoverNotifier.cpp:269
#, kde-format
msgid "Computer needs to restart"
msgstr "Komputilo bezonas rekomenci"

#: notifier/DiscoverNotifier.cpp:271
#, kde-format
msgid "Offline"
msgstr "Senrete"

#: notifier/DiscoverNotifier.cpp:273
#, kde-format
msgid "Applying unattended updates…"
msgstr "Aplikante neatentajn ĝisdatigojn..."

#: notifier/DiscoverNotifier.cpp:297
#, kde-format
msgid "Restart is required"
msgstr "Rekomenco estas postulata"

#: notifier/DiscoverNotifier.cpp:298
#, kde-format
msgid "The system needs to be restarted for the updates to take effect."
msgstr "La sistemo devas esti rekomencita por ke la ĝisdatigoj ekvalidu."

#: notifier/DiscoverNotifier.cpp:303
#, kde-format
msgctxt "@action:button"
msgid "Restart"
msgstr "Rekomenci"

#: notifier/DiscoverNotifier.cpp:328
#, kde-format
msgid "Upgrade available"
msgstr "Disponebla ĝisdatigo"

#: notifier/DiscoverNotifier.cpp:329
#, kde-format
msgctxt "A new distro release (name and version) is available for upgrade"
msgid "%1 is now available."
msgstr "%1 nun estas disponebla."

#: notifier/DiscoverNotifier.cpp:332
#, kde-format
msgctxt "@action:button"
msgid "Upgrade"
msgstr "Altgradigon"

#: notifier/main.cpp:38
#, kde-format
msgid "Discover Notifier"
msgstr "Discover-sciigilo"

#: notifier/main.cpp:40
#, kde-format
msgid "System update status notifier"
msgstr "Sistemĝisdatiga statosciigilo"

#: notifier/main.cpp:42
#, kde-format
msgid "© 2010-2024 Plasma Development Team"
msgstr "© 2010-2024 Plasma Disvolva Teamo"

#: notifier/main.cpp:46
#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Oliver Kellogg"

#: notifier/main.cpp:46
#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "okellogg@users.sourceforge.net"

#: notifier/main.cpp:51
#, kde-format
msgid "Replace an existing instance"
msgstr "Anstataŭigi ekzistantan petskribon"

#: notifier/main.cpp:53
#, kde-format
msgid "Do not show the notifier"
msgstr "Ne montri la sciigilon"

#: notifier/main.cpp:53
#, kde-format
msgid "hidden"
msgstr "kaŝita"

#: notifier/NotifierItem.cpp:22 notifier/NotifierItem.cpp:23
#, kde-format
msgid "Updates"
msgstr "Ĝisdatigoj"

#: notifier/NotifierItem.cpp:35
#, kde-format
msgid "Open Discover…"
msgstr "Malfermi Discover…"

#: notifier/NotifierItem.cpp:40
#, kde-format
msgid "See Updates…"
msgstr "Rigardi Ĝisdatigojn…"

#: notifier/NotifierItem.cpp:45
#, kde-format
msgid "Refresh…"
msgstr "Refreŝigi…"

#: notifier/NotifierItem.cpp:49
#, kde-format
msgid "Restart to apply installed updates"
msgstr "Rekomencu por apliki instalitajn ĝisdatigojn"

#: notifier/NotifierItem.cpp:50
#, kde-format
msgid "Click to restart the device"
msgstr "Klaku por rekomenci la aparaton"
