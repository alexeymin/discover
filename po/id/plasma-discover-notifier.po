# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Wantoyo <wantoyek@gmail.com>, 2017, 2018, 2019, 2020, 2021, 2022.
# Linerly <linerly@protonmail.com>, 2022, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-01-10 01:35+0000\n"
"PO-Revision-Date: 2023-02-17 17:12+0700\n"
"Last-Translator: Linerly <linerly@protonmail.com>\n"
"Language-Team: Indonesian <kde-i18n-doc@kde.org>\n"
"Language: id\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 22.12.2\n"

#: notifier/DiscoverNotifier.cpp:154
#, kde-format
msgid "View Updates"
msgstr "Tampilkan Pembaruan"

#: notifier/DiscoverNotifier.cpp:263
#, kde-format
msgid "Security updates available"
msgstr "Tersedia pembaruan keamanan"

#: notifier/DiscoverNotifier.cpp:265
#, kde-format
msgid "Updates available"
msgstr "Tersedia pembaruan"

#: notifier/DiscoverNotifier.cpp:267
#, kde-format
msgid "System up to date"
msgstr "Sistem terbarukan"

#: notifier/DiscoverNotifier.cpp:269
#, kde-format
msgid "Computer needs to restart"
msgstr "Komputer harus dimulai ulang"

#: notifier/DiscoverNotifier.cpp:271
#, kde-format
msgid "Offline"
msgstr "Luring"

#: notifier/DiscoverNotifier.cpp:273
#, kde-format
msgid "Applying unattended updates…"
msgstr "Menerapkan pembaruan lalai..."

#: notifier/DiscoverNotifier.cpp:297
#, kde-format
msgid "Restart is required"
msgstr "Harus dinyalakan ulang"

#: notifier/DiscoverNotifier.cpp:298
#, kde-format
msgid "The system needs to be restarted for the updates to take effect."
msgstr "Sistem perlu dinyalakan ulang supaya pembaruan berpengaruh."

#: notifier/DiscoverNotifier.cpp:303
#, kde-format
msgctxt "@action:button"
msgid "Restart"
msgstr "Nyalakan Ulang"

#: notifier/DiscoverNotifier.cpp:328
#, kde-format
msgid "Upgrade available"
msgstr "Tersedia peningkatan"

#: notifier/DiscoverNotifier.cpp:329
#, kde-format
msgctxt "A new distro release (name and version) is available for upgrade"
msgid "%1 is now available."
msgstr ""

#: notifier/DiscoverNotifier.cpp:332
#, kde-format
msgctxt "@action:button"
msgid "Upgrade"
msgstr "Tingkatkan"

#: notifier/main.cpp:38
#, kde-format
msgid "Discover Notifier"
msgstr "Penotifikasi Discover"

#: notifier/main.cpp:40
#, kde-format
msgid "System update status notifier"
msgstr "Penotifikasi status pembaruan sistem"

#: notifier/main.cpp:42
#, fuzzy, kde-format
#| msgid "© 2010-2022 Plasma Development Team"
msgid "© 2010-2024 Plasma Development Team"
msgstr "© 2010-2022 Tim Pengembangan Plasma"

#: notifier/main.cpp:46
#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Wantoyo,Linerly"

#: notifier/main.cpp:46
#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "wantoyek@gmail.com,linerly@protonmail.com"

#: notifier/main.cpp:51
#, kde-format
msgid "Replace an existing instance"
msgstr "Ganti sebuah instansi yang ada"

#: notifier/main.cpp:53
#, kde-format
msgid "Do not show the notifier"
msgstr "Jangan tampilkan penotifikasi"

#: notifier/main.cpp:53
#, kde-format
msgid "hidden"
msgstr "tersembunyi"

#: notifier/NotifierItem.cpp:22 notifier/NotifierItem.cpp:23
#, kde-format
msgid "Updates"
msgstr "Pembaruan"

#: notifier/NotifierItem.cpp:35
#, kde-format
msgid "Open Discover…"
msgstr "Buka Discover..."

#: notifier/NotifierItem.cpp:40
#, kde-format
msgid "See Updates…"
msgstr "Lihat Pembaruan..."

#: notifier/NotifierItem.cpp:45
#, kde-format
msgid "Refresh…"
msgstr "Segarkan..."

#: notifier/NotifierItem.cpp:49
#, kde-format
msgid "Restart to apply installed updates"
msgstr "Nyalakan ulang untuk menerapkan pembaruan yang terinstal"

#: notifier/NotifierItem.cpp:50
#, kde-format
msgid "Click to restart the device"
msgstr "Klik untuk memulai ulang peranti"

#~ msgid "New version: %1"
#~ msgstr "Versi baru: %1"
