# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: muon-discover\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-01-27 00:38+0000\n"
"PO-Revision-Date: 2023-02-15 16:05+0000\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-POFile-IgnoreConsistency: Launch\n"
"X-POFile-SpellExtra: Gonzalez Muon Discover Aleix Pol APT Buzz Jonathan\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: stdout appstream Hola Flatpak Jensen Turthra\n"
"X-POFile-IgnoreConsistency: Update\n"
"X-POFile-IgnoreConsistency: Name\n"
"X-POFile-IgnoreConsistency: Discover\n"
"X-POFile-SpellExtra: KNewStuff Graham Nate Leinir Snap pacman Arch\n"
"X-POFile-SpellExtra: PackageKit\n"

#: discover/DiscoverObject.cpp:190
#, kde-format
msgctxt "@title %1 is the distro name"
msgid ""
"%1 is not configured for installing apps through Discover—only app add-ons"
msgstr ""

#: discover/DiscoverObject.cpp:192
#, kde-kuit-format
msgctxt "@info:usagetip %1 is the distro name"
msgid ""
"To use Discover for apps, install your preferred module on the "
"<interface>Settings</interface> page, under <interface>Missing Backends</"
"interface>."
msgstr ""

#: discover/DiscoverObject.cpp:195
#, fuzzy, kde-format
#| msgid "Report This Issue"
msgctxt "@action:button %1 is the distro name"
msgid "Report This Issue to %1"
msgstr "Comunicar Este Problema"

#: discover/DiscoverObject.cpp:200
#, kde-kuit-format
msgctxt ""
"@info:usagetip %1 is the distro name; in this case it always contains 'Arch "
"Linux'"
msgid ""
"To use Discover for apps, install <link url='https://wiki.archlinux.org/"
"title/Flatpak#Installation'>Flatpak</link> or <link url='https://wiki."
"archlinux.org/title/KDE#Discover_does_not_show_any_applications'>PackageKit</"
"link> using the <command>pacman</command> package manager.<nl/><nl/> Review "
"<link url='https://archlinux.org/packages/extra/x86_64/discover/'>%1's "
"packaging for Discover</link>"
msgstr ""

#: discover/DiscoverObject.cpp:291
#, kde-format
msgid "Could not find category '%1'"
msgstr "Não foi possível encontrar a categoria '%1'"

#: discover/DiscoverObject.cpp:306
#, kde-format
msgid "Trying to open inexisting file '%1'"
msgstr "A tentar abrir o ficheiro inexistente '%1'"

#: discover/DiscoverObject.cpp:328
#, kde-format
msgid ""
"Cannot interact with flatpak resources without the flatpak backend %1. "
"Please install it first."
msgstr ""
"Não é possível interagir com os recursos de Flatpak sem a infra-estrutura de "
"Flatpak %1. Instale-a primeiro, por favor."

#: discover/DiscoverObject.cpp:332
#, kde-format
msgid "Could not open %1"
msgstr "Não foi possível abrir o %1"

#: discover/DiscoverObject.cpp:394
#, kde-format
msgid "Please make sure Snap support is installed"
msgstr "Por favor, certifique-se que o suporte de Snap está instalado"

#: discover/DiscoverObject.cpp:396
#, kde-format
msgid ""
"Could not open %1 because it was not found in any available software "
"repositories."
msgstr ""
"Não foi possível aceder ao %1, porque não foi encontrado nos repositórios de "
"aplicações disponíveis."

#: discover/DiscoverObject.cpp:399
#, kde-format
msgid "Please report this issue to the packagers of your distribution."
msgstr ""
"Por favor comunique este problema aos gestores de pacotes da sua "
"distribuição."

#: discover/DiscoverObject.cpp:402
#, kde-format
msgid "Report This Issue"
msgstr "Comunicar Este Problema"

#: discover/DiscoverObject.cpp:464 discover/DiscoverObject.cpp:466
#: discover/main.cpp:117
#, kde-format
msgid "Discover"
msgstr "Discover"

#: discover/DiscoverObject.cpp:467
#, kde-format
msgid ""
"Discover was closed before certain tasks were done, waiting for it to finish."
msgstr ""
"O Discover foi fechado antes que algumas tarefas terminem - à espera que ele "
"finalize."

#: discover/main.cpp:33
#, kde-format
msgid "Directly open the specified application by its appstream:// URI."
msgstr ""
"Abre directamente a aplicação indicada de acordo com o seu URI do "
"appstream://."

#: discover/main.cpp:34
#, kde-format
msgid "Open with a search for programs that can deal with the given mimetype."
msgstr ""
"Abrir com uma pesquisa por programas que consigam lidar com o tipo MIME "
"indicado."

#: discover/main.cpp:35
#, kde-format
msgid "Display a list of entries with a category."
msgstr "Mostrar uma lista de elementos com uma dada categoria."

#: discover/main.cpp:36
#, kde-format
msgid "Open Discover in a said mode. Modes correspond to the toolbar buttons."
msgstr ""
"Abre o Discover num modo seguro. Os modos correspondem aos botões da barra "
"de ferramentas."

#: discover/main.cpp:37
#, kde-format
msgid "List all the available modes."
msgstr "Mostra todos os modos disponíveis."

#: discover/main.cpp:38
#, kde-format
msgid "Local package file to install"
msgstr "O ficheiro local do pacote a instalar"

#: discover/main.cpp:39
#, kde-format
msgid "List all the available backends."
msgstr "Mostra todas as infra-estruturas disponíveis."

#: discover/main.cpp:40
#, kde-format
msgid "Search string."
msgstr "Procurar pelo texto."

#: discover/main.cpp:41
#, kde-format
msgid "Lists the available options for user feedback"
msgstr "Apresenta as opções disponíveis para as reacções dos utilizadores"

#: discover/main.cpp:43
#, kde-format
msgid "Supports appstream: url scheme"
msgstr "Suporta o esquema de URL's 'appstream:'"

#: discover/main.cpp:119
#, kde-format
msgid "An application explorer"
msgstr "Um explorador de aplicações"

#: discover/main.cpp:121
#, fuzzy, kde-format
#| msgid "© 2010-2022 Plasma Development Team"
msgid "© 2010-2024 Plasma Development Team"
msgstr "© 2010-2022 da Equipa de Desenvolvimento do Plasma"

#: discover/main.cpp:122
#, kde-format
msgid "Aleix Pol Gonzalez"
msgstr "Aleix Pol Gonzalez"

#: discover/main.cpp:123
#, kde-format
msgid "Nate Graham"
msgstr "Nate Graham"

#: discover/main.cpp:124
#, kde-format
msgid "Quality Assurance, Design and Usability"
msgstr "Controlo de Qualidade, Desenho e Usabilidade"

#: discover/main.cpp:128
#, kde-format
msgid "Dan Leinir Turthra Jensen"
msgstr "Dan Leinir Turthra Jensen"

#: discover/main.cpp:129
#, kde-format
msgid "KNewStuff"
msgstr "KNewStuff"

#: discover/main.cpp:136
#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "José Nuno Pires"

#: discover/main.cpp:136
#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "zepires@gmail.com"

#: discover/main.cpp:149
#, kde-format
msgid "Available backends:\n"
msgstr "Infra-estruturas disponíveis:\n"

#: discover/main.cpp:202
#, kde-format
msgid "Available modes:\n"
msgstr "Modos disponíveis:\n"

#: discover/qml/AddonsView.qml:28 discover/qml/Navigation.qml:58
#, kde-format
msgid "Addons for %1"
msgstr "Extensões de %1"

#: discover/qml/AddonsView.qml:80
#, kde-format
msgid "More…"
msgstr "Mais…"

#: discover/qml/AddonsView.qml:89
#, kde-format
msgid "Apply Changes"
msgstr "Aplicar as Alterações"

#: discover/qml/AddonsView.qml:97
#, kde-format
msgid "Reset"
msgstr "Reiniciar"

#: discover/qml/AddSourceDialog.qml:21
#, kde-format
msgid "Add New %1 Repository"
msgstr "Adicionar um Novo Repositório de %1"

#: discover/qml/AddSourceDialog.qml:45
#, kde-format
msgid "Add"
msgstr "Adicionar"

#: discover/qml/AddSourceDialog.qml:50 discover/qml/DiscoverWindow.qml:271
#: discover/qml/InstallApplicationButton.qml:46
#: discover/qml/ProgressView.qml:139 discover/qml/SourcesPage.qml:201
#: discover/qml/UpdatesPage.qml:259 discover/qml/WebflowDialog.qml:41
#, kde-format
msgid "Cancel"
msgstr "Cancelar"

#: discover/qml/ApplicationDelegate.qml:177
#: discover/qml/ApplicationPage.qml:223
#, kde-format
msgid "%1 rating"
msgid_plural "%1 ratings"
msgstr[0] "%1 classificação"
msgstr[1] "%1 classificações"

#: discover/qml/ApplicationDelegate.qml:177
#: discover/qml/ApplicationPage.qml:223
#, kde-format
msgid "No ratings yet"
msgstr "Ainda sem classificações"

#: discover/qml/ApplicationPage.qml:67
#, kde-format
msgctxt ""
"@item:inlistbox %1 is the name of an app source e.g. \"Flathub\" or \"Ubuntu"
"\""
msgid "From %1"
msgstr ""

#: discover/qml/ApplicationPage.qml:83
#, kde-format
msgid "%1 - %2"
msgstr "%1 - %2"

#: discover/qml/ApplicationPage.qml:202
#, kde-format
msgid "Unknown author"
msgstr "Autor desconhecido"

#: discover/qml/ApplicationPage.qml:247
#, kde-format
msgid "Version:"
msgstr "Versão:"

#: discover/qml/ApplicationPage.qml:259
#, fuzzy, kde-format
#| msgid "Size"
msgid "Size:"
msgstr "Tamanho"

#: discover/qml/ApplicationPage.qml:271
#, fuzzy, kde-format
#| msgid "License"
#| msgid_plural "Licenses"
msgid "License:"
msgid_plural "Licenses:"
msgstr[0] "Licença"
msgstr[1] "Licenças"

#: discover/qml/ApplicationPage.qml:279
#, kde-format
msgctxt "The app does not provide any licenses"
msgid "Unknown"
msgstr "Desconhecida"

#: discover/qml/ApplicationPage.qml:313
#, kde-format
msgid "What does this mean?"
msgstr "O que significa isto?"

#: discover/qml/ApplicationPage.qml:322
#, kde-format
msgid "See more…"
msgid_plural "See more…"
msgstr[0] "Ver mais…"
msgstr[1] "Ver mais…"

#: discover/qml/ApplicationPage.qml:333
#, fuzzy, kde-format
#| msgid "Content Rating"
msgid "Content Rating:"
msgstr "Classificação do Conteúdo"

#: discover/qml/ApplicationPage.qml:342
#, kde-format
msgid "Age: %1+"
msgstr "Idade: %1+"

#: discover/qml/ApplicationPage.qml:362
#, kde-format
msgctxt "@action"
msgid "See details…"
msgstr "Ver os detalhes…"

#: discover/qml/ApplicationPage.qml:384
#, kde-format
msgctxt "@info placeholder message"
msgid "Screenshots not available for %1"
msgstr ""

#: discover/qml/ApplicationPage.qml:556
#, kde-format
msgid "Documentation"
msgstr "Documentação"

#: discover/qml/ApplicationPage.qml:557
#, kde-format
msgid "Read the project's official documentation"
msgstr "Ler a documentação oficial do projecto"

#: discover/qml/ApplicationPage.qml:573
#, kde-format
msgid "Website"
msgstr "Página Web"

#: discover/qml/ApplicationPage.qml:574
#, kde-format
msgid "Visit the project's website"
msgstr "Visitar a paǵina Web do projecto"

#: discover/qml/ApplicationPage.qml:590
#, kde-format
msgid "Addons"
msgstr "Extensões"

#: discover/qml/ApplicationPage.qml:591
#, kde-format
msgid "Install or remove additional functionality"
msgstr "Instalar ou remover as funcionalidades adicionais"

#: discover/qml/ApplicationPage.qml:610
#, kde-format
msgctxt "Exports the application's URL to an external service"
msgid "Share"
msgstr "Partilhar"

#: discover/qml/ApplicationPage.qml:611
#, kde-format
msgid "Send a link for this application"
msgstr "Enviar uma ligação para esta aplicação"

#: discover/qml/ApplicationPage.qml:627
#, kde-format
msgctxt "The subject line for an email. %1 is the name of an application"
msgid "Check out the %1 app!"
msgstr "Veja a aplicação %1!"

#: discover/qml/ApplicationPage.qml:647
#, kde-format
msgid "What's New"
msgstr "O Que Há de Novo"

#: discover/qml/ApplicationPage.qml:677
#, kde-format
msgid "Reviews"
msgstr "Revisões"

#: discover/qml/ApplicationPage.qml:689
#, kde-format
msgid "Loading reviews for %1"
msgstr "A carregar as revisões do %1"

#: discover/qml/ApplicationPage.qml:697
#, kde-format
msgctxt "@info placeholder message"
msgid "Reviews for %1 are temporarily unavailable"
msgstr ""

#: discover/qml/ApplicationPage.qml:721
#, fuzzy, kde-format
#| msgid "Show all %1 Reviews"
#| msgid_plural "Show all %1 Reviews"
msgctxt "@action:button"
msgid "Show All Reviews"
msgstr "Mostrar Todas as Revisões do %1"

#: discover/qml/ApplicationPage.qml:733
#, kde-format
msgid "Write a Review"
msgstr "Escrever uma Revisão"

#: discover/qml/ApplicationPage.qml:733
#, kde-format
msgid "Install to Write a Review"
msgstr "Instalar para Escrever uma Revisão"

#: discover/qml/ApplicationPage.qml:745
#, kde-format
msgid "Get Involved"
msgstr "Envolva-se"

#: discover/qml/ApplicationPage.qml:787
#, kde-format
msgid "Donate"
msgstr "Doar"

#: discover/qml/ApplicationPage.qml:788
#, kde-format
msgid "Support and thank the developers by donating to their project"
msgstr ""
"Dê algum apoio e agradeça aos programadores, fazendo uma doação ao projecto "
"deles"

#: discover/qml/ApplicationPage.qml:804
#, kde-format
msgid "Report Bug"
msgstr "Comunicar um Erro"

#: discover/qml/ApplicationPage.qml:805
#, kde-format
msgid "Log an issue you found to help get it fixed"
msgstr "Comunique um problema que tenha detectado para que o corrijam"

#: discover/qml/ApplicationPage.qml:821
#, kde-format
msgid "Contribute"
msgstr "Contribuir"

#: discover/qml/ApplicationPage.qml:822
#, kde-format
msgid "Help the developers by coding, designing, testing, or translating"
msgstr "Ajude os programadores com código, desenhos, testes ou traduções"

#: discover/qml/ApplicationPage.qml:854
#, kde-format
msgid "All Licenses"
msgstr "Todas as Licenças"

#: discover/qml/ApplicationPage.qml:887
#, kde-format
msgid "Content Rating"
msgstr "Classificação do Conteúdo"

#: discover/qml/ApplicationPage.qml:904
#, kde-format
msgid "Risks of proprietary software"
msgstr "Riscos do 'software' proprietário"

#: discover/qml/ApplicationPage.qml:910
#, kde-kuit-format
msgctxt "@info"
msgid ""
"This application's source code is partially or entirely closed to public "
"inspection and improvement. That means third parties and users like you "
"cannot verify its operation, security, and trustworthiness, or modify and "
"redistribute it without the authors' express permission.<nl/><nl/>The "
"application may be perfectly safe to use, or it may be acting against you in "
"various ways—such as harvesting your personal information, tracking your "
"location, or transmitting the contents of your files to someone else. There "
"is no easy way to be sure, so you should only install this application if "
"you fully trust its authors (<link url='%1'>%2</link>).<nl/><nl/>You can "
"learn more at <link url='%3'>%3</link>."
msgstr ""
"O código-fonte desta aplicação está parcial ou totalmente fechado para a "
"inspecção ou melhorias do público. Isto significa que terceiros ou os "
"utilizadores como você não conseguem verificar a sua operação, segurança e "
"fidedignidade, ou ainda modificá-lo e redistribuí-lo sem a autorização "
"expressa dos autores.<nl/><nl/>A aplicação poderá ser perfeitamente segura "
"de usar, ou poderá actuar contra si de várias formas - como recolher os seus "
"dados pessoais, seguir a sua localização ou transmitir o conteúdo dos seus "
"ficheiros para alguém. Não existe forma simples de ter a certeza, pelo que "
"só deveria instalar esta aplicação se confiar por completo nos seus autores "
"(<link url='%1'>%2</link>).<nl/><nl/>Poderá aprender mais em <link url='%3'>"
"%3</link>."

#: discover/qml/ApplicationPage.qml:911
#, kde-kuit-format
msgctxt "@info"
msgid ""
"This application's source code is partially or entirely closed to public "
"inspection and improvement. That means third parties and users like you "
"cannot verify its operation, security, and trustworthiness, or modify and "
"redistribute it without the authors' express permission.<nl/><nl/>The "
"application may be perfectly safe to use, or it may be acting against you in "
"various ways—such as harvesting your personal information, tracking your "
"location, or transmitting the contents of your files to someone else. There "
"is no easy way to be sure, so you should only install this application if "
"you fully trust its authors (%1).<nl/><nl/>You can learn more at <link "
"url='%2'>%2</link>."
msgstr ""
"O código-fonte desta aplicação está parcial ou totalmente fechado para a "
"inspecção ou melhorias do público. Isto significa que terceiros ou os "
"utilizadores como você não conseguem verificar a sua operação, segurança e "
"fidedignidade, ou ainda modificá-lo e redistribuí-lo sem a autorização "
"expressa dos autores.<nl/><nl/>A aplicação poderá ser perfeitamente segura "
"de usar, ou poderá actuar contra si de várias formas - como recolher os seus "
"dados pessoais, seguir a sua localização ou transmitir o conteúdo dos seus "
"ficheiros para alguém. Não existe forma simples de ter a certeza, pelo que "
"só deveria instalar esta aplicação se confiar por completo nos seus autores "
"(%1).<nl/><nl/>Poderá aprender mais em <link url='%2'>%2</link>."

#: discover/qml/ApplicationsListPage.qml:53
#, kde-format
msgid "Search: %2 - %3 item"
msgid_plural "Search: %2 - %3 items"
msgstr[0] "Procurar: %2 - %3 item"
msgstr[1] "Procurar: %2 - %3 itens"

#: discover/qml/ApplicationsListPage.qml:55
#, kde-format
msgid "Search: %1"
msgstr "Procurar: %1"

#: discover/qml/ApplicationsListPage.qml:59
#, kde-format
msgid "%2 - %1 item"
msgid_plural "%2 - %1 items"
msgstr[0] "%2 - %1 item"
msgstr[1] "%2 - %1 itens"

#: discover/qml/ApplicationsListPage.qml:65
#, kde-format
msgid "Search - %1 item"
msgid_plural "Search - %1 items"
msgstr[0] "Procurar - %1 item"
msgstr[1] "Procurar - %1 itens"

#: discover/qml/ApplicationsListPage.qml:67
#: discover/qml/ApplicationsListPage.qml:254
#, kde-format
msgid "Search"
msgstr "Procurar"

#: discover/qml/ApplicationsListPage.qml:98 discover/qml/ReviewsPage.qml:99
#, kde-format
msgid "Sort: %1"
msgstr "Ordenação: %1"

#: discover/qml/ApplicationsListPage.qml:103
#, kde-format
msgctxt "Search results most relevant to the search query"
msgid "Relevance"
msgstr ""

#: discover/qml/ApplicationsListPage.qml:114
#, kde-format
msgid "Name"
msgstr "Nome"

#: discover/qml/ApplicationsListPage.qml:125 discover/qml/Rating.qml:119
#, kde-format
msgid "Rating"
msgstr "Classificação"

#: discover/qml/ApplicationsListPage.qml:136
#, kde-format
msgid "Size"
msgstr "Tamanho"

#: discover/qml/ApplicationsListPage.qml:147
#, kde-format
msgid "Release Date"
msgstr "Data de Lançamento"

#: discover/qml/ApplicationsListPage.qml:200
#, kde-format
msgid "Nothing found"
msgstr "Nada foi encontrado"

#: discover/qml/ApplicationsListPage.qml:208
#, kde-format
msgctxt "@action:button"
msgid "Search in All Categories"
msgstr "Procurar em Todas as Categorias"

#: discover/qml/ApplicationsListPage.qml:218
#, kde-format
msgctxt "@action:button %1 is the name of an application"
msgid "Search the Web for \"%1\""
msgstr "Procurar na Web por \"%1\""

#: discover/qml/ApplicationsListPage.qml:222
#, kde-format
msgctxt ""
"If appropriate, localize this URL to be something more relevant to the "
"language. %1 is the text that will be searched for."
msgid "https://duckduckgo.com/?q=%1"
msgstr "https://duckduckgo.com/?q=%1"

#: discover/qml/ApplicationsListPage.qml:233
#, kde-format
msgctxt ""
"@info:placeholder %1 is the name of an application; %2 is the name of a "
"category of apps or add-ons"
msgid "\"%1\" was not found in the \"%2\" category"
msgstr "O \"%1\" não foi encontrado na categoria \"%2\""

#: discover/qml/ApplicationsListPage.qml:235
#, kde-format
msgctxt "@info:placeholder %1 is the name of an application"
msgid "\"%1\" was not found in the available sources"
msgstr "O \"%1\" não foi encontrado nas fontes disponíveis"

#: discover/qml/ApplicationsListPage.qml:236
#, fuzzy, kde-format
#| msgctxt "@info:placeholder%1 is the name of an application"
#| msgid ""
#| "\"%1\" may be available on the web. Software acquired from the web has "
#| "not been reviewed by your distributor for functionality or stability. Use "
#| "with caution."
msgctxt "@info:placeholder %1 is the name of an application"
msgid ""
"\"%1\" may be available on the web. Software acquired from the web has not "
"been reviewed by your distributor for functionality or stability. Use with "
"caution."
msgstr ""
"O \"%1\" poderá estar disponível na Web. As aplicações adquiridas na Web não "
"foram validadas pelo seu distribuidor no que respeita à funcionalidade ou "
"estabilidade. Use com cuidado."

#: discover/qml/ApplicationsListPage.qml:269
#, kde-format
msgid "Still looking…"
msgstr "Ainda à procura…"

#: discover/qml/BrowsingPage.qml:20
#, fuzzy, kde-format
#| msgid "&Home"
msgctxt "@title:window the name of a top-level 'home' page"
msgid "Home"
msgstr "&Início"

#: discover/qml/BrowsingPage.qml:64
#, kde-format
msgid "Unable to load applications"
msgstr "Não foi possível carregar as aplicações"

#: discover/qml/BrowsingPage.qml:99
#, kde-format
msgctxt "@title:group"
msgid "Most Popular"
msgstr "Mais Populares"

#: discover/qml/BrowsingPage.qml:121
#, kde-format
msgctxt "@title:group"
msgid "Newly Published & Recently Updated"
msgstr ""

#: discover/qml/BrowsingPage.qml:160
#, kde-format
msgctxt "@title:group"
msgid "Editor's Choice"
msgstr "Escolha do Editor"

#: discover/qml/BrowsingPage.qml:177
#, kde-format
msgctxt "@title:group"
msgid "Highest-Rated Games"
msgstr "Jogos Melhor Classificados"

#: discover/qml/BrowsingPage.qml:197 discover/qml/BrowsingPage.qml:228
#, kde-format
msgctxt "@action:button"
msgid "See More"
msgstr "Ver Mais"

#: discover/qml/BrowsingPage.qml:208
#, kde-format
msgctxt "@title:group"
msgid "Highest-Rated Developer Tools"
msgstr "Ferramentas de Desenvolvimento Melhor Classificadas"

#: discover/qml/CarouselDelegate.qml:212
#, kde-format
msgctxt "@action:button Start playing media"
msgid "Play"
msgstr ""

#: discover/qml/CarouselDelegate.qml:214
#, kde-format
msgctxt "@action:button Pause any media that is playing"
msgid "Pause"
msgstr ""

#: discover/qml/CarouselMaximizedViewContent.qml:40
#, kde-format
msgctxt "@action:button"
msgid "Switch to Overlay"
msgstr ""

#: discover/qml/CarouselMaximizedViewContent.qml:42
#, kde-format
msgctxt "@action:button"
msgid "Switch to Full Screen"
msgstr ""

#: discover/qml/CarouselMaximizedViewContent.qml:75
#, kde-format
msgctxt ""
"@action:button Close overlay/window/popup with carousel of screenshots"
msgid "Close"
msgstr ""

#: discover/qml/CarouselNavigationButton.qml:56
#, kde-format
msgctxt "@action:button"
msgid "Previous Screenshot"
msgstr ""

#: discover/qml/CarouselNavigationButton.qml:56
#, kde-format
msgctxt "@action:button"
msgid "Next Screenshot"
msgstr ""

#: discover/qml/DiscoverWindow.qml:43
#, kde-format
msgid "Running as <em>root</em> is discouraged and unnecessary."
msgstr "A execução <em>root</em> não é recomendada nem necessária."

#: discover/qml/DiscoverWindow.qml:58
#, kde-format
msgid "&Home"
msgstr "&Início"

#: discover/qml/DiscoverWindow.qml:68
#, kde-format
msgid "&Search"
msgstr "&Procurar"

#: discover/qml/DiscoverWindow.qml:76
#, kde-format
msgid "&Installed"
msgstr "&Instalado"

#: discover/qml/DiscoverWindow.qml:87
#, kde-format
msgid "Fetching &updates…"
msgstr "A obter as act&ualizações…"

#: discover/qml/DiscoverWindow.qml:87
#, fuzzy, kde-format
#| msgctxt "Update section name"
#| msgid "&Update (%1)"
msgid "&Update (%1)"
msgid_plural "&Updates (%1)"
msgstr[0] "Act&ualização (%1)"
msgstr[1] "Act&ualização (%1)"

#: discover/qml/DiscoverWindow.qml:95
#, kde-format
msgid "&About"
msgstr "&Acerca"

#: discover/qml/DiscoverWindow.qml:103
#, kde-format
msgid "S&ettings"
msgstr "&Configuração"

#: discover/qml/DiscoverWindow.qml:156 discover/qml/DiscoverWindow.qml:340
#: discover/qml/DiscoverWindow.qml:454
#, kde-format
msgid "Error"
msgstr "Erro"

#: discover/qml/DiscoverWindow.qml:160
#, kde-format
msgid "Unable to find resource: %1"
msgstr "Não foi possível descobrir o recurso: %1"

#: discover/qml/DiscoverWindow.qml:258 discover/qml/SourcesPage.qml:195
#, kde-format
msgid "Proceed"
msgstr "Prosseguir"

#: discover/qml/DiscoverWindow.qml:316
#, kde-format
msgid "Report this issue"
msgstr "Comunicar este problema"

#: discover/qml/DiscoverWindow.qml:340
#, kde-format
msgid "Error %1 of %2"
msgstr "Erro %1 de %2"

#: discover/qml/DiscoverWindow.qml:390
#, kde-format
msgctxt "@action:button"
msgid "Show Previous"
msgstr "Mostrar o Anterior"

#: discover/qml/DiscoverWindow.qml:403
#, kde-format
msgctxt "@action:button"
msgid "Show Next"
msgstr "Mostrar o Seguinte"

#: discover/qml/DiscoverWindow.qml:419
#, kde-format
msgid "Copy to Clipboard"
msgstr "Copiar para a Área de Transferência"

#: discover/qml/Feedback.qml:14
#, kde-format
msgid "Submit usage information"
msgstr "Enviar as informações de utilização"

#: discover/qml/Feedback.qml:15
#, kde-format
msgid ""
"Sends anonymized usage information to KDE so we can better understand our "
"users. For more information see https://kde.org/privacypolicy-apps.php."
msgstr ""
"Envia informações de utilização anónimas para o KDE, para que possamos "
"compreender melhor os nossos utilizadores. Para mais informações, veja em "
"https://kde.org/privacypolicy-apps.php."

#: discover/qml/Feedback.qml:19
#, kde-format
msgid "Submitting usage information…"
msgstr "A enviar informações de utilização…"

#: discover/qml/Feedback.qml:19
#, kde-format
msgid "Configure"
msgstr "Configurar"

#: discover/qml/Feedback.qml:23
#, kde-format
msgid "Configure feedback…"
msgstr "Configurar as reacções…"

#: discover/qml/Feedback.qml:30 discover/qml/SourcesPage.qml:22
#, kde-format
msgid "Configure Updates…"
msgstr "Configurar as Actualizações…"

#: discover/qml/Feedback.qml:58
#, kde-format
msgid ""
"You can help us improving this application by sharing statistics and "
"participate in surveys."
msgstr ""
"Poder-nos-á ajudar a melhor esta aplicação, partilhando estatísticas e "
"participando em inquéritos."

#: discover/qml/Feedback.qml:58
#, kde-format
msgid "Contribute…"
msgstr "Contribuir…"

#: discover/qml/Feedback.qml:63
#, kde-format
msgid "We are looking for your feedback!"
msgstr "Estamos à procura das suas reacções!"

#: discover/qml/Feedback.qml:63
#, kde-format
msgid "Participate…"
msgstr "Participar…"

#: discover/qml/InstallApplicationButton.qml:24
#, kde-format
msgctxt "State being fetched"
msgid "Loading…"
msgstr "A carregar…"

#: discover/qml/InstallApplicationButton.qml:28
#, fuzzy, kde-format
#| msgid "Install"
msgctxt "@action:button %1 is the name of a software repository"
msgid "Install from %1"
msgstr "Instalar"

#: discover/qml/InstallApplicationButton.qml:30
#, kde-format
msgctxt "@action:button"
msgid "Install"
msgstr "Instalar"

#: discover/qml/InstallApplicationButton.qml:32
#, kde-format
msgid "Remove"
msgstr "Remover"

#: discover/qml/InstalledPage.qml:14
#, kde-format
msgid "Installed"
msgstr "Instalado"

#: discover/qml/Navigation.qml:34
#, kde-format
msgid "Resources for '%1'"
msgstr "Recursos de '%1'"

#: discover/qml/ProgressView.qml:18
#, kde-format
msgid "Tasks (%1%)"
msgstr "Tarefas (%1%)"

#: discover/qml/ProgressView.qml:18 discover/qml/ProgressView.qml:45
#, kde-format
msgid "Tasks"
msgstr "Tarefas"

#: discover/qml/ProgressView.qml:113
#, kde-format
msgctxt "TransactioName - TransactionStatus: speed, remaining time"
msgid "%1 - %2: %3, %4 remaining"
msgstr "%1 - %2: %3, falta %4"

#: discover/qml/ProgressView.qml:121
#, kde-format
msgctxt "TransactioName - TransactionStatus: speed"
msgid "%1 - %2: %3"
msgstr "%1 - %2: %3"

#: discover/qml/ProgressView.qml:128
#, kde-format
msgctxt "TransactioName - TransactionStatus"
msgid "%1 - %2"
msgstr "%1 - %2"

#: discover/qml/ReviewDelegate.qml:65
#, kde-format
msgid "unknown reviewer"
msgstr "revisor desconhecido"

#: discover/qml/ReviewDelegate.qml:66
#, kde-format
msgid "<b>%1</b> by %2"
msgstr "<b>%1</b> de %2"

#: discover/qml/ReviewDelegate.qml:66
#, kde-format
msgid "Comment by %1"
msgstr "Comentário de %1"

#: discover/qml/ReviewDelegate.qml:84
#, kde-format
msgid "Version: %1"
msgstr "Versão: %1"

#: discover/qml/ReviewDelegate.qml:84
#, kde-format
msgid "Version: unknown"
msgstr "Versão: desconhecida"

#: discover/qml/ReviewDelegate.qml:100
#, kde-format
msgid "Votes: %1 out of %2"
msgstr "Votos: %1 em %2"

#: discover/qml/ReviewDelegate.qml:107
#, kde-format
msgid "Was this review useful?"
msgstr "Esta revisão foi útil?"

#: discover/qml/ReviewDelegate.qml:119
#, kde-format
msgctxt "Keep this string as short as humanly possible"
msgid "Yes"
msgstr "Sim"

#: discover/qml/ReviewDelegate.qml:136
#, kde-format
msgctxt "Keep this string as short as humanly possible"
msgid "No"
msgstr "Não"

#: discover/qml/ReviewDialog.qml:20
#, kde-format
msgid "Reviewing %1"
msgstr "A rever o '%1'"

#: discover/qml/ReviewDialog.qml:27
#, kde-format
msgid "Submit review"
msgstr "Enviar a revisão"

#: discover/qml/ReviewDialog.qml:40
#, kde-format
msgid "Rating:"
msgstr "Classificação:"

#: discover/qml/ReviewDialog.qml:45
#, kde-format
msgid "Name:"
msgstr "Nome:"

#: discover/qml/ReviewDialog.qml:53
#, kde-format
msgid "Title:"
msgstr "Título:"

#: discover/qml/ReviewDialog.qml:73
#, kde-format
msgid "Enter a rating"
msgstr "Indique uma classificação"

#: discover/qml/ReviewDialog.qml:76
#, kde-format
msgid "Write the title"
msgstr "Escreva o título"

#: discover/qml/ReviewDialog.qml:79
#, kde-format
msgid "Write the review"
msgstr "Escreva a revisão"

#: discover/qml/ReviewDialog.qml:82
#, kde-format
msgid "Keep writing…"
msgstr "Continue a escrever"

#: discover/qml/ReviewDialog.qml:85
#, kde-format
msgid "Too long!"
msgstr "Demasiado longo!"

#: discover/qml/ReviewDialog.qml:88
#, kde-format
msgctxt "@info:usagetip"
msgid "Insert a name"
msgstr "Inserir um nome"

#: discover/qml/ReviewsPage.qml:54
#, kde-format
msgid "Reviews for %1"
msgstr "Revisões do %1"

#: discover/qml/ReviewsPage.qml:62
#, kde-format
msgid "Write a Review…"
msgstr "Escrever uma Revisão…"

#: discover/qml/ReviewsPage.qml:74
#, kde-format
msgid "Install this app to write a review"
msgstr "Instale esta aplicação para escrever uma revisão"

#: discover/qml/ReviewsPage.qml:103
#, kde-format
msgctxt "@label:listbox Most relevant reviews"
msgid "Most Relevant"
msgstr ""

#: discover/qml/ReviewsPage.qml:110
#, kde-format
msgctxt "@label:listbox Most recent reviews"
msgid "Most Recent"
msgstr ""

#: discover/qml/ReviewsPage.qml:117
#, fuzzy, kde-format
#| msgctxt "@title:group"
#| msgid "Highest-Rated Games"
msgctxt "@label:listbox Reviews with the highest ratings"
msgid "Highest Ratings"
msgstr "Jogos Melhor Classificados"

#: discover/qml/ReviewsStats.qml:53
#, fuzzy, kde-format
#| msgid "Reviews"
msgctxt "how many reviews"
msgid "%1 reviews"
msgstr "Revisões"

#: discover/qml/ReviewsStats.qml:76
#, kde-format
msgctxt "widest character in the language"
msgid "M"
msgstr ""

#: discover/qml/ReviewsStats.qml:154
#, fuzzy, kde-format
#| msgid "unknown reviewer"
msgid "Unknown reviewer"
msgstr "revisor desconhecido"

#: discover/qml/ReviewsStats.qml:175
#, kde-format
msgctxt "Opening upper air quote"
msgid "“"
msgstr ""

#: discover/qml/ReviewsStats.qml:190
#, kde-format
msgctxt "Closing lower air quote"
msgid "„"
msgstr ""

#: discover/qml/SearchField.qml:26
#, kde-format
msgid "Search…"
msgstr "Procurar…"

#: discover/qml/SearchField.qml:26
#, kde-format
msgid "Search in '%1'…"
msgstr "Procurar no '%1'…"

#: discover/qml/SourcesPage.qml:18
#, kde-format
msgid "Settings"
msgstr "Configuração"

#: discover/qml/SourcesPage.qml:110
#, kde-format
msgid "Default source"
msgstr "Fonte predefinida"

#: discover/qml/SourcesPage.qml:118
#, kde-format
msgid "Add Source…"
msgstr "Adicionar uma Fonte…"

#: discover/qml/SourcesPage.qml:145
#, kde-format
msgid "Make default"
msgstr "Tornar predefinido"

#: discover/qml/SourcesPage.qml:248
#, kde-format
msgid "Increase priority"
msgstr "Aumentar a prioridade"

#: discover/qml/SourcesPage.qml:254
#, kde-format
msgid "Failed to increase '%1' preference"
msgstr "Não foi possível aumentar a performance do '%1'"

#: discover/qml/SourcesPage.qml:260
#, kde-format
msgid "Decrease priority"
msgstr "Diminuir a prioridade"

#: discover/qml/SourcesPage.qml:266
#, kde-format
msgid "Failed to decrease '%1' preference"
msgstr "Não foi possível diminuir a performance do '%1'"

#: discover/qml/SourcesPage.qml:272
#, kde-format
msgid "Remove repository"
msgstr "Remover o repositório"

#: discover/qml/SourcesPage.qml:283
#, kde-format
msgid "Show contents"
msgstr "Mostrar o conteúdo"

#: discover/qml/SourcesPage.qml:324
#, kde-format
msgid "Missing Backends"
msgstr "Infra-Estruturas em Falta"

#: discover/qml/UpdatesPage.qml:13
#, kde-format
msgid "Updates"
msgstr "Actualizações"

#: discover/qml/UpdatesPage.qml:46
#, kde-format
msgid "Update Issue"
msgstr "Problema na Actualização"

#: discover/qml/UpdatesPage.qml:46
#, kde-format
msgid "Technical details"
msgstr "Detalhes técnicos"

#: discover/qml/UpdatesPage.qml:62
#, kde-format
msgid "There was an issue installing this update. Please try again later."
msgstr ""
"Ocorreu um problema ao instalar esta actualização. Por favor, tente de novo "
"mais tarde."

#: discover/qml/UpdatesPage.qml:68
#, kde-format
msgid "See Technical Details"
msgstr "Ver os Detalhes Técnicos"

#: discover/qml/UpdatesPage.qml:95
#, kde-format
msgctxt "@info %1 is the name of the user's distro/OS"
msgid ""
"If the error indicated above looks like a real issue and not a temporary "
"network error, please report it to %1, not KDE."
msgstr ""

#: discover/qml/UpdatesPage.qml:103
#, kde-format
msgid "Copy Text"
msgstr ""

#: discover/qml/UpdatesPage.qml:107
#, kde-format
msgctxt "@info %1 is the name of the user's distro/OS"
msgid "Error message copied. Remember to report it to %1, not KDE!"
msgstr ""

#: discover/qml/UpdatesPage.qml:114
#, fuzzy, kde-format
#| msgid "Report This Issue"
msgctxt "@action:button %1 is the name of the user's distro/OS"
msgid "Report Issue to %1"
msgstr "Comunicar Este Problema"

#: discover/qml/UpdatesPage.qml:140
#, fuzzy, kde-format
#| msgid "Update Selected"
msgctxt "@action:button as in, 'update the selected items' "
msgid "Update Selected"
msgstr "Actualizar os Seleccionados"

#: discover/qml/UpdatesPage.qml:140
#, fuzzy, kde-format
#| msgid "Update All"
msgctxt "@action:button as in, 'update all items'"
msgid "Update All"
msgstr "Actualizar Tudo"

#: discover/qml/UpdatesPage.qml:180
#, kde-format
msgid "Ignore"
msgstr "Ignorar"

#: discover/qml/UpdatesPage.qml:227
#, kde-format
msgid "Select All"
msgstr "Seleccionar Tudo"

#: discover/qml/UpdatesPage.qml:235
#, kde-format
msgid "Select None"
msgstr "Deseleccionar Tudo"

#: discover/qml/UpdatesPage.qml:242
#, kde-format
msgid "Restart automatically after update has completed"
msgstr "Reiniciar automaticamente após terminar a actualização"

#: discover/qml/UpdatesPage.qml:249
#, kde-format
msgid "Total size: %1"
msgstr "Tamanho total: %1"

#: discover/qml/UpdatesPage.qml:284
#, kde-format
msgid "Restart Now"
msgstr "Reiniciar Agora"

#: discover/qml/UpdatesPage.qml:413
#, kde-format
msgid "Installing"
msgstr "A Instalar"

#: discover/qml/UpdatesPage.qml:444
#, kde-format
msgid "Update from:"
msgstr "Actualizar de:"

#: discover/qml/UpdatesPage.qml:456
#, kde-format
msgctxt ""
"%1 is the backend that provides this app, %2 is the specific repository or "
"address within that backend"
msgid "%1 (%2)"
msgstr "%1 (%2)"

#: discover/qml/UpdatesPage.qml:463
#, kde-format
msgid "More Information…"
msgstr "Mais Informações…"

#: discover/qml/UpdatesPage.qml:491
#, kde-format
msgctxt "@info"
msgid "Fetching updates…"
msgstr "A obter as actualizações…"

#: discover/qml/UpdatesPage.qml:504
#, kde-format
msgctxt "@info"
msgid "Updates"
msgstr "Actualizações"

#: discover/qml/UpdatesPage.qml:513
#, kde-format
msgctxt "@info"
msgid "Restart the system to complete the update process"
msgstr "Reiniciar o sistema para terminar o processo de actualização"

#: discover/qml/UpdatesPage.qml:525 discover/qml/UpdatesPage.qml:532
#: discover/qml/UpdatesPage.qml:539
#, kde-format
msgctxt "@info"
msgid "Up to date"
msgstr "Actualizado"

#: discover/qml/UpdatesPage.qml:546
#, kde-format
msgctxt "@info"
msgid "Should check for updates"
msgstr "Se deve procurar por actualizações"

#: discover/qml/UpdatesPage.qml:553
#, kde-format
msgctxt "@info"
msgid "Time of last update unknown"
msgstr "Hora da última actualização desconhecida"

#~ msgid "Compact Mode (auto/compact/full)."
#~ msgstr "Modo Compacto (auto/compacto/completo)."

#~ msgid "%1"
#~ msgstr "%1"

#~ msgid ""
#~ "Discover currently cannot be used to install any apps or perform system "
#~ "updates because none of its app backends are available."
#~ msgstr ""
#~ "O Discover não consegue de momento ser usado para instalar nenhumas "
#~ "aplicações, dado que nenhuma das suas infra-estruturas de aplicações "
#~ "estão disponíveis."

#~ msgctxt "@info"
#~ msgid ""
#~ "You can install some on the Settings page, under the <interface>Missing "
#~ "Backends</interface> section.<nl/><nl/>Also please consider reporting "
#~ "this as a packaging issue to your distribution."
#~ msgstr ""
#~ "Poderá instalar alguns na página de Configuração, sob a secção "
#~ "<interface>Infra-Estruturas em Falta</interface>.<nl/><nl/>Pense também "
#~ "em relatar isto como um problema no pacote da sua distribuição."

#~ msgctxt "@info"
#~ msgid ""
#~ "You can use <command>pacman</command> to install the optional "
#~ "dependencies that are needed to enable the application backends.<nl/><nl/"
#~ ">Please note that Arch Linux developers recommend using <command>pacman</"
#~ "command> for managing software because the PackageKit backend is not well-"
#~ "integrated on Arch Linux."
#~ msgstr ""
#~ "Poderá usar o <command>pacman</command> para instalar as dependências "
#~ "opcionais que são necessárias para activar as infra-estruturas da "
#~ "aplicação.<nl/><nl/>Lembre-se também que os programadores do Arch Linux "
#~ "recomendam a utilização do <command>pacman</command> para gerir as "
#~ "aplicações, porque a infra-estrutura de PackageKit não está bem integrada "
#~ "no Arch Linux."

#~ msgid "Learn More"
#~ msgstr "Saber Mais"

#~ msgid "Could not access the screenshots"
#~ msgstr "Não foi possível aceder às imagens"

#~ msgid "&Up to date"
#~ msgstr "Act&ualizado"

#~ msgid ""
#~ "If you would like to report the update issue to your distribution's "
#~ "packagers, include this information:"
#~ msgstr ""
#~ "Se desejar comunicar o problema na actualização aos criadores de pacotes "
#~ "da sua distribuição, inclua esta informação:"

#~ msgid "Error message copied to clipboard"
#~ msgstr "A mensagem de erro foi copiada para a área de transferência"

#~ msgid "Sources"
#~ msgstr "Fontes"

#~ msgid "Distributed by"
#~ msgstr "Distribuído por"
